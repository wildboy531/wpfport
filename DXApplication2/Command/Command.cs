﻿using DevExpress.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DXApplication2.Command
{
    public class Command : ICommand
    {
        public virtual event EventHandler CanExecuteChanged;

        public virtual bool CanExecute(object parameter)
        {
            throw new NotImplementedException();
        }

        public virtual void Execute(object parameter)
        {
            throw new NotImplementedException();
        }
    }
    public class LinkCommand : Command
    {
        private string url = "";

        public string Url { get => url; set => url = value; }

        public LinkCommand()
        {

        }
        public LinkCommand(string url)
        {
            Url = url;
        }

        public override event  EventHandler CanExecuteChanged;

        public override bool CanExecute(object parameter)
        {
            return true;
        }

        public override void Execute(object parameter)
        {
            Console.WriteLine("======================LinkCommand 실행===========================");
            System.Diagnostics.ProcessStartInfo pro = new System.Diagnostics.ProcessStartInfo("chrome.exe", "https://documentation.devexpress.com/WpfThemeEditor/10429/WPF-Theme-Editor"); 
            System.Diagnostics.ProcessStartInfo pro1 = new System.Diagnostics.ProcessStartInfo("chrome.exe", "https://documentation.devexpress.com/WpfThemeDesigner/118707/WPF-Theme-Designer");
            System.Diagnostics.Process.Start(pro);
            System.Diagnostics.Process.Start(pro1);

        }
    }
    public class Commands : ObservableCollection<Command>
    {
        public Commands(Command command)
        {
            Add(command);
        }
    }
}
