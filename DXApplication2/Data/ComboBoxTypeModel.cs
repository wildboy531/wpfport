﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXApplication2.Data
{
    public class ComboBoxTypeModel : ObservableCollection<string>
    {
        public class Type
        {
            private string _type;
            public string type { get => _type; set => _type = value; }
        }
        public ComboBoxTypeModel()
        {
            Add("Ballast");
            Add("Loading");
            Add("Dischg.");
            Add("Bunker");
            Add("Canel");
            Add("Passing");
        }
    }
}
