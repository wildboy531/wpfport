﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXApplication2.Data
{
    public class PortRotationData : INotifyPropertyChanged
    {
        int? index;
        string type;
        string name;
        bool? secaport;
        double? distance;
        double? seca;
        double? wf;
        double? spd;
        double? sea;
        double? ldrate;
        double? idle;
        double? work;
        double? dem;
        double? des;
        double? portcharge;
        DateTime? arrival;
        DateTime? departure;
        string standarddate;

        // Constructor
        public PortRotationData(int index, string type, string name, bool secaport, double? distace, double? seca, double? wf, double? spd, double? sea, double? ldrate, 
            double? idle, double? work, double? dem, double? des, double? portcharge, DateTime? arrival, DateTime? depature, string standarddate){
            Index = index;
            Type = type;
            Name = name;
            Secaport = secaport;
            Distance = distace;
            Seca = seca;
            Wf = wf;
            Spd = spd;
            Sea = sea;
            Ldrate = ldrate;
            Idle = idle;
            Work = work;
            Dem = dem;
            Des = des;
            Portcharge = portcharge;
            Arrival = arrival;
            Departure = departure;
            Standarddate = standarddate;
        }
        public PortRotationData()
        {
            Name = "";
        }
        // Encapsule
        public int? Index { get => index; set { index = value; OnPropertyChanged("Index"); } }
        public string Type { get => type; set { type = value; OnPropertyChanged("Type"); } }
        public string Name { get => name; set { name = value; OnPropertyChanged("Name"); } }
        public bool? Secaport { get => secaport; set { secaport = value; OnPropertyChanged("Secaport"); } }
        public double? Wf { get => wf; set { wf = value; OnPropertyChanged("Wf"); } }
        public double? Spd { get => spd; set { spd = value; OnPropertyChanged("Spd"); } }
        public double? Sea { get => sea; set { sea = value; OnPropertyChanged("Sea"); } }
        public double? Ldrate { get => ldrate; set { ldrate = value; OnPropertyChanged("Ldrate"); } }
        public double? Idle { get => idle; set { idle = value; OnPropertyChanged("Idle"); } }
        public double? Work { get => work; set { work = value; OnPropertyChanged("Work"); } }
        public double? Dem { get => dem; set { dem = value; OnPropertyChanged("Dem"); } }
        public double? Des { get => des; set { des = value; OnPropertyChanged("Des"); } }
        public double? Portcharge { get => portcharge; set { portcharge = value; OnPropertyChanged("Portcharge"); } }
        public DateTime? Arrival { get => arrival; set { arrival = value; OnPropertyChanged("Arrival"); } }
        public DateTime? Departure { get => departure; set { departure = value; OnPropertyChanged("Depature"); } }
        public double? Distance { get => distance; set { distance = value; OnPropertyChanged("Distance"); } }
        public double? Seca { get => seca; set { seca = value; OnPropertyChanged("Seca"); } }
        public string Standarddate { get => standarddate; set { standarddate = value; OnPropertyChanged("Standarddate"); } }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(String name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

    }
    public class PortRotationList : ObservableCollection<PortRotationData>
    {
        DateTime datetime = new DateTime();
        public PortRotationList()
        {
            Add(new PortRotationData(1, "Ballast", "", false, null, null, null, null, null, null, null, null, null, null, null, null, null,""));
            Add(new PortRotationData(2, "", "", false, null, null, null, null, null, null, null, null, null, null, null, null, null, ""));
            Add(new PortRotationData(3, "", "", false, null, null, null, null, null, null, null, null, null, null, null, null, null, ""));
            Add(new PortRotationData(4, "", "", false, null, null, null, null, null, null, null, null, null, null, null, null, null, ""));
            Add(new PortRotationData(0, "Margin", "", false, null, null, null, null, null, null, null, null, null, null, null, null, null, ""));
        }
        public void AddList(int AIndex)
        {
            if (AIndex >= 0 && AIndex <= this.Count)
            {
                this.Insert(AIndex + 1, new PortRotationData());
            }
        }

        public void Append()
        {
            this.Insert(this.Count-1,new PortRotationData());
        }

        public void Remove(int AIndex)
        {
            if (AIndex >= 0 && AIndex < this.Count)
            {
                this.RemoveAt(AIndex);
            }
            if (AIndex ==0)
            {
                this[AIndex].Type = "Ballast";
            }
            if (this.Count < 5)
            {
                this.Append();
            }
        }

        public void ClearData()
        {
            this.Clear();
            Add(new PortRotationData(1, "Ballast", "", false, null, null, null, null, null, null, null, null, null, null, null, null, null, ""));
            Add(new PortRotationData(2, "", "", false, null, null, null, null, null, null, null, null, null, null, null, null, null, ""));
            Add(new PortRotationData(3, ".", "", false, null, null, null, null, null, null, null, null, null, null, null, null, null, ""));
            Add(new PortRotationData(4, "", "", false, null, null, null, null, null, null, null, null, 3, null, null, null, null, ""));
            Add(new PortRotationData(0, "Margin", "", false, null, null, null, null, null, null, null, null, null, null, null, null, null, ""));
        }

        public void MoveDown(int AIndex)
        {
            if (AIndex >= 0 && AIndex <= this.Count - 2)
            {
                this.Move(AIndex, AIndex + 1);
            }
        }

        public void MoveUp(int AIndex)
        {
            if (AIndex >= 1 && AIndex <= this.Count - 1)
            {
                this.Move(AIndex, AIndex - 1);
            }
        }

        public double? NullValidate(double? Value)
        {
            double? Result = 0;
            if(Value == null)
            {
                return Result;
            }
            else
            {
                return Value;
            }
        }

        public void CalcData()
        {            
            for (int i = 0; i < this.Count; i++)
            {
                PortRotationData Port = this[i];
                double? spd = NullValidate(Port.Spd);
                double? wf = NullValidate(Port.Wf);
                double? distance = NullValidate(Port.Distance);
                double? sea = NullValidate(Port.Sea);
                double? idle = NullValidate(Port.Idle);
                double? work = NullValidate(Port.Work);      
                
                if (!(spd == 0)) { Port.Sea = ((distance * (1 + wf / 100)) / spd) / 24; }
                if (this[i].Name !="")
                {
                    if (i == 0 && Port.Departure != null)
                    {
                        Port.Arrival = this[i].Departure.Value.AddDays(-(double)(idle + work));
                    }
                    if (i > 0)
                    {
                        if (this[i - 1].Departure != null)
                        {
                            Port.Arrival = this[i - 1].Departure.Value.AddDays((double)sea);
                        }
                        if (Port.Arrival != null)
                        {
                            Port.Departure = Port.Arrival.Value.AddDays((double)(idle + work));
                        }
                    }
                }             
            }
        }
        public void SetIndex()
        { int index = 1;
            for (int i = 0; i < this.Count -1; i++)
            {   
                PortRotationData Port = this[i];
                Port.Index = index;
                index++;
            }
        }
        public void StandardDateSet()
        {
            int StandardDateIndex = 100;
            for (int i = 0; i < this.Count -1; i++)
            {
                this[i].Standarddate = "";
            }
            for (int i = 0; i < this.Count -1; i++)
            {
                if(this[i].Name !=""){
                    StandardDateIndex = i;
                    break;
                }
            }
            if (this[StandardDateIndex].Type =="Ballast")
            {
                this[StandardDateIndex].Standarddate = "Departure";
            }
            else
            {
                this[StandardDateIndex].Standarddate = "Arrival";
            }
        }
    }
}
