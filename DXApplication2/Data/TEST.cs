﻿using System;
using System.ComponentModel;


namespace DXApplication2.Data
{
    public class TEST : INotifyPropertyChanged
    {
        int id;
        string name;

        public event PropertyChangedEventHandler PropertyChanged;

        public TEST()
        {
        }

        public TEST(int iD, string name)
        {
            ID = iD;
            Name = name;
        }

        public int ID
        {
            get { return id; }
            set
            {
                if (id == value)
                    return;
                id = value;
                OnPropertyChanged("ID");            }
        }
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
