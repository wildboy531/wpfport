﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections;

namespace DXApplication2.Data
{
    public class TESTLIST :ObservableCollection<TEST>
    {
        public TESTLIST()
        {
            Add(new TEST(1, "T"));
            Add(new TEST(3, "TTT"));
            Add(new TEST(5, "TTTTT"));
            Add(new TEST(1, "T"));
        }
    }
}
