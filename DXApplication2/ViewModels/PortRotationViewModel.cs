﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using System.ComponentModel;
using DXApplication2.Data;
using DevExpress.Mvvm.POCO;
using DXApplication2.Command;
using System.Windows.Input;

namespace DXApplication2.ViewModels
{
    [POCOViewModel]
    public class PortRotationViewModel 
    {
        private PortRotationList PList;

        private ICommand command;
        public virtual ICommand LinkCommand
        {
            get { return command; }
            set
            {
                 command = value;
            }
        }
        public virtual PortRotationList Lists
        {
            get => PList;
            set => PList = value;
        }
        //RaisePropertyChanged("Index");
        //RaisePropertyChanged("Type");
        //RaisePropertyChanged("Name");
        //RaisePropertyChanged("Secaport");
        //RaisePropertyChanged("WF");
        //RaisePropertyChanged("Spd");
        //RaisePropertyChanged("Sea");
        //RaisePropertyChanged("Ldrate");
        //RaisePropertyChanged("Idle");
        //RaisePropertyChanged("Work");
        //RaisePropertyChanged("Dem");
        //RaisePropertyChanged("Des");
        //RaisePropertyChanged("Des");
        //RaisePropertyChanged("Portcharge");
        //RaisePropertyChanged("Arrival");
        //RaisePropertyChanged("Depature");            
        public virtual ComboBoxTypeModel type
        {
            get; set;
        }
        
        public virtual PortRotationData data
        {
            get;set;
        }
        //#region INotifyPropertiyChanged Member
        //public event PropertyChangedEventHandler PropertyChanged;
        //private void RaisePropertyChanged(string propertyName)
        //{
        //    PropertyChangedEventHandler handler = PropertyChanged;
        //    if (handler != null)
        //    {
        //        handler(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}
        //#endregion
       
        public PortRotationViewModel()
        {
            Lists = new PortRotationList();
            type = new ComboBoxTypeModel();
            command = new LinkCommand(); // 어떤 변수에 할당하냐는 관계없이 POCO에 의해 View와는 자동 Binding 된다.
            data = new PortRotationData();
            
        }

        public static PortRotationViewModel Create()
        {
            return ViewModelSource.Create(() => new PortRotationViewModel());
        }
        public void AddList(int AIndex)
        {
            Lists.AddList(AIndex);
            Lists.SetIndex();
        }

        public void Append()
        {
            Lists.Append();
            Lists.SetIndex();
        }

        public void Remove(int AIndex)
        {
            Lists.Remove(AIndex);
            Lists.StandardDateSet();
            Lists.SetIndex();
        }

        public void MoveUp(int AIndex)
        {
            Lists.MoveUp(AIndex);
            Lists.SetIndex();
        }

        public void MoveDown(int AIndex)
        {
            Lists.MoveDown(AIndex);
            Lists.SetIndex();
        }

        public void ClearData()
        {
            Lists.ClearData();
            Lists.SetIndex();
        }

        public void CalcData()
        {
            Lists.StandardDateSet();
            Lists.CalcData();
        }
    }
}