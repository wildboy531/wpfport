﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;

using DXApplication2.Data;
using DevExpress.Mvvm.POCO;

namespace DXApplication2.ViewModels
{
    [POCOViewModel]
    public class TESTLISTViewModel
    {
        public virtual TESTLIST TESTs { get; set; } // Data Schema (Model)
        public TESTLISTViewModel()
        {
            TESTs = new TESTLIST();
        }
        public static TESTLISTViewModel Create()
        {
            return ViewModelSource.Create(() => new TESTLISTViewModel());
        }
    }
}