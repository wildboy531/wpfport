﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using System.ComponentModel;
using DXApplication2.Data;

namespace DXApplication2.ViewModels
{
    [POCOViewModel]
    public class ViewModel1 
    {
        public virtual TEST test { get; set; }

        //public virtual int ID { get; set; }

        //public virtual string Name { get; set; }

        public static ViewModel1 Create()
        {
            return ViewModelSource.Create(() => new ViewModel1());
        }
        
        public ViewModel1()
        {
            test = new TESTLIST()[3];
        }

    }
}