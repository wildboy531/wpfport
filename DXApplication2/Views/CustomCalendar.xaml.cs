﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DXApplication2.Views
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public DateTime? TimeValue;
        public Window1()
        {
            InitializeComponent();
        }

        public void OK_Click(object sender, RoutedEventArgs e)
        {
            DateTime? date = calendar.SelectedDate;
            DateTime time;            

            if (date != null) 
            {
                time = (DateTime)date;
                time = time.AddHours(Convert.ToDouble(Hour.Text));
                time = time.AddMinutes(Convert.ToDouble(Minute.Text));
                TimeValue = time;
                PortRotationView Main = ((MainWindow)Application.Current.MainWindow).PortRotationView1;
                Main.PortRotationGrid.SetCellValue(Main.PortRotationTable.FocusedRowHandle, Main.PortRotationTable.FocusedColumn, TimeValue);
                Main.PortRotationTable.CommitEditing();
                this.Close();               
            }
            else
            {
                TimeValue = null;
                 this.Close();                
            }
           
        }

        private void Calcel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
