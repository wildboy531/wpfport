﻿using DevExpress.Utils;
using DevExpress.Xpf.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DXApplication2.Views
{
    /// <summary>
    /// Interaction logic for PortRotationView.xaml
    /// </summary>
    public partial class PortRotationView : UserControl
    {
        string arrival = " ";
        string departure = " ";
        double total = 0;
        double Ballast = 0;
        double Laden = 0;
        double seca = 0;
        double Port = 0;
        private static object LightGreen = (new BrushConverter()).ConvertFrom("#e2efe2");
        private static object LightBlue = (new BrushConverter()).ConvertFrom("#f3f5ff");
        private static object LightBoldGreen = (new BrushConverter()).ConvertFrom("#c2dac2");
        private static object LightYellow = (new BrushConverter()).ConvertFrom("#fefecb");
        private static object LightGray = (new BrushConverter()).ConvertFrom("#FFECECEC");

        public PortRotationView()
        {
            InitializeComponent();
        }
        private bool IsFocusable(int rowHandle, ColumnBase column)
        {
            String port = PortRotationGrid.GetCellValue(rowHandle, Name) as String;
            String operationType = PortRotationGrid.GetCellValue(rowHandle, Type) as String;
            //Index
            if (column == Index)
            {
                return false;
            }
            // Type
            if (column == Type)
            {
                if (rowHandle == 0 || rowHandle == PortRotationGrid.VisibleRowCount - 1)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            // Name
            if (column == Name)
            {
                if (rowHandle == PortRotationGrid.VisibleRowCount - 1)
                {
                    return false;
                }
                return true;
            }
            //Port SECA
            if (column == Secaport && port == "")
            {
                return false;
            }
            // Distance, SECA, W.F, Speed
            if ((column == Distance) || (column == Seca) || (column == Wf) || (column == Spd))
            {
                if ((rowHandle == 0) || String.IsNullOrEmpty(port) || !HasPrevPort(rowHandle))
                    return false;
            }
            // Sea
            if (column == Sea)
            {
                return false;
            }
            // LD Rate, Shore, Dem, Des, Port Charge
            if ((column == Ldrate) || (column == Crane) || (column == Dem) || (column == Des) || (column == Portcharge))
            {
                if (String.IsNullOrEmpty(port))
                    return false;
            }
            // Idle, Work
            if ((column == Idle) || (column == Work))
            {
                if (String.IsNullOrEmpty(port))
                    return false;
            }
            // Arrival
            if (column == Arrival)
            {
                if (String.IsNullOrEmpty(port) || HasPrevPort(rowHandle) || (operationType.Equals("Ballast")))
                    return false;
            }
            // Departure
            if (column == Departure)
            {
                if (String.IsNullOrEmpty(port) || HasPrevPort(rowHandle) || (!operationType.Equals("Ballast")))
                    return false;
            }
            return true;
        }

        private bool HasPrevPort(int rowHandle)
        {
            for (int i = rowHandle - 1; i >= 0; i--)
            {
                String port = PortRotationGrid.GetCellValue(i, Name) as String;
                if (!String.IsNullOrEmpty(port))
                    return true;
            }
            return false;
        }

        private void FocusChanged()
        {
            foreach (GridColumn column in PortRotationTable.VisibleColumns)
            {
                bool isFocusable = IsFocusable(PortRotationTable.FocusedRowHandle, column);

                column.AllowEditing = isFocusable ? DefaultBoolean.True : DefaultBoolean.False;
                column.AllowFocus = isFocusable;
            }
        }
        private object GetDefaultColor(int rowHandle, ColumnBase column)
        {
            String port = PortRotationGrid.GetCellValue(rowHandle, Name) as String;
            String type = PortRotationGrid.GetCellValue(rowHandle, Type) as String;
            if (column == Distance)
                return LightYellow;
            else if (column == Seca)
                return LightGreen;
            else if ((column == Type && port != "" && type != "") || column == Arrival || column == Departure)
                return LightBlue;
            else
                return Brushes.White;
        }

        private object GetBlockedColor(int rowHandle, ColumnBase column)
        {
            String port = PortRotationGrid.GetCellValue(rowHandle, Name) as String;
            if ((column == Sea) && (rowHandle > 0) && !String.IsNullOrEmpty(port) && HasPrevPort(rowHandle))
                return LightYellow;
            else
                return LightBlue;
        }
        private void TableView_CustomCellAppearance(object sender, DevExpress.Xpf.Grid.CustomCellAppearanceEventArgs e)
        {
            if (e.Property == TextElement.BackgroundProperty)
            {
                if ((PortRotationGrid.CurrentColumn == e.Column) && (PortRotationTable.FocusedRowHandle == e.RowHandle))
                {
                    if (e.IsEditorVisible)
                        e.Result = Brushes.White;
                    else
                        e.Result = LightBoldGreen;
                }
                else if (IsFocusable(e.RowHandle, e.Column))
                    e.Result = GetDefaultColor(e.RowHandle, e.Column);
                else
                    e.Result = GetBlockedColor(e.RowHandle, e.Column);
                if (e.Column == Index)
                {
                    e.Result = LightGray;
                }
                e.Handled = true;
            }
        }

        private void PortRotationGrid_CurrentColumnChanged(object sender, CurrentColumnChangedEventArgs e)
        {
            FocusChanged();
            PortRotationTable.CommitEditing();
        }

        private void PortRotationGrid_CurrentItemChanged(object sender, CurrentItemChangedEventArgs e)
        {
            FocusChanged();
            PortRotationTable.CommitEditing();
        }

        private void PortRotationGrid_CustomSummary(object sender, DevExpress.Data.CustomSummaryEventArgs e)
        {
        
            if (((GridSummaryItem)e.Item).FieldName == "Arrival")
            {                
                for (int i = 0; i < PortRotationGrid.VisibleRowCount - 1; i++)
                {
                    if (!HasPrevPort(i))
                    {
                        e.TotalValue = PortRotationGrid.GetCellValue(i, Arrival);
                        PortRotationTable.CommitEditing();
                        if (e.TotalValue != null)
                        {
                            arrival = e.TotalValue.ToString();
                        }
                        break;
                    }
                }
            }
            if (((GridSummaryItem)e.Item).FieldName == "Departure")
            {
                for (int i = PortRotationGrid.VisibleRowCount - 1; i > 0; i--)
                {
                    if (PortRotationGrid.GetCellValue(i, Departure) != null)
                    {
                        e.TotalValue = PortRotationGrid.GetCellValue(i, Departure);
                        PortRotationTable.CommitEditing();
                        if (e.TotalValue != null)
                        {
                            departure = e.TotalValue.ToString();
                        }
                        break;
                    }
                }
            }            
            if (TotalDurations != null && ((GridSummaryItem)e.Item).FieldName == "Departure")
            {
                string totalduration = string.Format("Total Duration : {0} Days (Ballast: {1},Laden: {2}, Seca: {3}, Port: {4}) / ", total, Ballast, Laden, seca, Port) + arrival + " ~ " + departure;
                TotalDurations.Text = totalduration;
            }
        }

        private void PortRotationTable_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (PortRotationTable.IsEditing == true)
                {
                    PortRotationTable.CommitEditing();
                    if (PortRotationGrid.CurrentColumn == Name)
                    {
                        PortRotationTable.MoveNextRow();
                        if (PortRotationTable.FocusedRowHandle == PortRotationGrid.VisibleRowCount - 2)
                        {
                            return;
                        }
                    }
                    else if (PortRotationGrid.CurrentColumn == Portcharge)
                    {
                        PortRotationGrid.CurrentColumn = Ldrate;
                        PortRotationTable.MoveNextRow();
                    }
                    else
                    {
                        PortRotationTable.MoveNextCell();
                    }
                }
            }
        }

        private void PortRotationTable_TextInput(object sender, TextCompositionEventArgs e)
        {
            if (PortRotationGrid.CurrentColumn == Dem)
            {
                PortRotationGrid.SetCellValue(PortRotationTable.FocusedRowHandle,Des, null);
            }
            if (PortRotationGrid.CurrentColumn == Des)
            {
                PortRotationGrid.SetCellValue(PortRotationTable.FocusedRowHandle,Dem, null);
            }
        }

        private void PART_Item_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            Window1 CustomCalendar = new Window1();
            var point = button.PointToScreen(new Point(0, 0));
            CustomCalendar.Top = point.Y + 25;
            CustomCalendar.Left = point.X - 127;
            CustomCalendar.Show();           
        }
    }
}
