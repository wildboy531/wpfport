﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DXApplication2.Views
{
    /// <summary>
    /// Interaction logic for TESTLIST.xaml
    /// </summary>
    public partial class TESTLIST : UserControl
    {
        public TESTLIST()
        {
            InitializeComponent();
        }

        private void TableView_AddingNewRow(object sender, System.ComponentModel.AddingNewEventArgs e)
        {
            
        }

        private void TableView_CellValueChanged(object sender, DevExpress.Xpf.Grid.CellValueChangedEventArgs e)
        {
            //Command -> ViewModel
        }
    }
}
